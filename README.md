# PUCRS - Inteligência Artificial - T1

[Este repositório](https://gitlab.com/debemdeboas/pucrs-inteligencia-artificial-t1) contém
os fontes para a resolução do primeiro trabalho da cadeira de Inteligência Artificial do curso
de Ciência de Computação da PUC-RS do semestre 2022/2.

O relatório [encontra-se aqui](https://debemdeboas.gitlab.io/pucrs-inteligencia-artificial-t1/relatorio_ia.pdf).

## Execução

Antes de executar o programa é necessário instalar os pacotes em `requirements.txt`.

```sh
python -m pip install -r requirements.txt
```

Instalados, basta rodar `python main.py` e seguir as instruções na tela.

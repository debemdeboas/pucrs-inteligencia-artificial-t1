
import random
import time
import signal

from copy import deepcopy
from dataclasses import dataclass
from enum import Enum, auto
from typing import NamedTuple

from rich.align import Align
from rich.box import ROUNDED
from rich.console import Console, Group
from rich.layout import Layout
from rich.live import Live
from rich.panel import Panel
from rich.pretty import Pretty
from rich.table import Table


LABYRINTH_FILE = 'labirinto5.txt'
GENERATE_TABLE_WITH_POS = False
WRITE_LOGS = True
ITERATION_INTERVAL = 0.1
MAX_POPULATION = 30
MAX_STEPS = 1200
ELITISM_BEST_AMOUNT = 15
MAX_GENERATIONS = 1000

console = Console()


class Movement(Enum):
    Left      = auto()
    Right     = auto()
    Up        = auto()
    Down      = auto()
    UpLeft    = auto()
    UpRight   = auto()
    DownLeft  = auto()
    DownRight = auto()

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return self.name


class Cell(Enum):
    Empty = auto()
    Wall  = auto()
    Food  = auto()
    Eaten = auto()
    Entry = auto()

    def __str__(self) -> str:
        match self:
            case Cell.Empty:
                return ' '
            case Cell.Wall:
                return '[bright_white]X[/bright_white]'
            case Cell.Food:
                return '[sky_blue2]F[sky_blue2/]'
            case Cell.Eaten:
                return '[bold green]F[/bold green]'
            case Cell.Entry:
                return '[gold1]E[/gold1]'


class LabyrinthResult(NamedTuple):
    food: int
    steps: int


class Point(NamedTuple):
    i: int
    j: int


@dataclass
class Labyrinth:
    n: int
    cells: list[list[Cell]]

    def __getitem__(self, index) -> list[Cell]:
        return self.cells[index]

    def generate_table(self, pos: Point) -> Table:
        if GENERATE_TABLE_WITH_POS:
            return self.generate_table_with_coord(pos)
        else:
            return self.generate_table_no_coord(pos)

    def generate_table_no_coord(self, pos: Point) -> Table:
        table = Table(show_header=False, show_footer=False, box=ROUNDED, show_lines=True)
        for i, line in enumerate(self.cells):
            row = []
            for j, cell in enumerate(line):
                if i == pos.i and j == pos.j:
                    row.append(f'[default on yellow]{cell}[/default on yellow]')
                else:
                    row.append(f'{cell}')
            table.add_row(*row)
        return table

    def generate_table_with_coord(self, pos: Point) -> Table:
        table = Table(show_header=False, show_footer=False, box=ROUNDED, show_lines=True)
        for i, line in enumerate(self.cells):
            row = []
            for j, cell in enumerate(line):
                if i == pos.i and j == pos.j:
                    row.append(f'[default on yellow]({i}, {j}) {cell}[/default on yellow]')
                else:
                    row.append(f'({i}, {j}) {cell}')
            table.add_row(*row)
        return table

    def print(self):
        console.print(self.generate_table(Point(-1, -1)))


def write_log(f, line: str, console_log=False):
    if console_log:
        console.log(line)
    if not WRITE_LOGS:
        return
    f.write(line + '\n')


def move(curr_pos: Point, movement: Movement) -> Point:
    match movement:
        case Movement.Left:
            return Point(curr_pos.i, curr_pos.j - 1)
        case Movement.Right:
            return Point(curr_pos.i, curr_pos.j + 1)
        case Movement.Up:
            return Point(curr_pos.i - 1, curr_pos.j)
        case Movement.Down:
            return Point(curr_pos.i + 1, curr_pos.j)
        case Movement.UpLeft:
            return Point(curr_pos.i - 1, curr_pos.j - 1)
        case Movement.UpRight:
            return Point(curr_pos.i - 1, curr_pos.j + 1)
        case Movement.DownLeft:
            return Point(curr_pos.i + 1, curr_pos.j - 1)
        case Movement.DownRight:
            return Point(curr_pos.i + 1, curr_pos.j + 1)


def heuristic(lab: Labyrinth, pos: Point) -> Movement:
    # If there's food in the next (neighboring) cell and we haven't eaten it, go there.
    # If not, choose a direction randomly

    movements = list(dict(Movement.__members__).values())
    random.shuffle(movements)

    candidates: list[tuple[int, Movement]] = []

    for movement in movements:
        next_pos = move(pos, movement)

        if not (next_pos.i >= 0 and next_pos.j >= 0):
            continue

        try:
            cell = lab[next_pos.i][next_pos.j]
        except IndexError:
            # Invalid movement
            continue

        match cell:
            case Cell.Food:
                return movement
            case Cell.Empty:
                candidates.append((0, movement))
            case Cell.Eaten:
                candidates.append((1, movement))
            case Cell.Entry:
                candidates.append((2, movement))

    candidates.sort(key=lambda x: x[0])

    if len(candidates):
        # Try again but this time prefer non-wall cells
        next_moves = [candidates.pop(0)]
        if len(candidates):
            next_moves.append(candidates.pop(0))
        random.shuffle(next_moves)
        return next_moves[0][1]

    return random.choice(list(v for v in dict(Movement.__members__).values()))


def solve_step_by_step(labyrinth: Labyrinth,
                       total_food: int,
                       max_steps: int,
                       sleep_interval: float = ITERATION_INTERVAL,
                       inherited_steps: list[Movement] = []) -> tuple[LabyrinthResult, list[Movement]]:
    """
    Essa função só é usada para imprimir na tela o passo-a-passo da resolução do algoritmo.
    """
    steps: list[Movement] = []
    tried_steps = 0
    food_collected = 0
    curr_pos = Point(0, 0)
    tried_before = set()
    iter_inherited_steps = iter(inherited_steps)

    layout = Layout(name='root')
    layout.split_row(
        Layout(name='Labyrinth'),
        Layout(name='Movements')
    )
    layout['Labyrinth'].ratio = 3
    step_history = []

    layout['Labyrinth'].update(Panel(labyrinth.generate_table(curr_pos), title='Labyrinth'))
    layout['Movements'].update(Table.grid())

    with Live(layout, auto_refresh=True, refresh_per_second=60, screen=True) as live:
        tried_before = set()
        while tried_steps < max_steps and food_collected < total_food:
            layout['Labyrinth'].update(Panel(labyrinth.generate_table(curr_pos), title='Labyrinth'))

            next_move = next(iter_inherited_steps, heuristic(labyrinth, curr_pos))
            next_pos = move(curr_pos, next_move)

            if next_pos in tried_before:
                continue

            if not (next_pos.i >= 0 and next_pos.j >= 0):
                continue

            tried_steps += 1

            # Validate new position
            try:
                cell = labyrinth[next_pos.i][next_pos.j]
            except IndexError:
                # Invalid movement
                tried_before.add(next_pos)
                continue

            match cell:
                case Cell.Food:
                    labyrinth[next_pos.i][next_pos.j] = Cell.Eaten
                    food_collected += 1
                case Cell.Wall:
                    # Invalid
                    tried_before.add(next_pos)
                    continue
                case Cell.Empty | Cell.Entry:
                    # Do nothing
                    pass

            tried_before = set()

            step_history.append(f'[blue]{next_move.name}[/blue] to [yellow]{next_pos}[/yellow]')
            step_history_table = Table.grid()
            step_history_table.width = 80
            for i, _steps in enumerate(reversed(step_history)):
                if i > 11:
                    break
                step_history_table.add_row(_steps)

            log_group = Group(
                Panel(Pretty(curr_pos, justify='center'), title='Current position'),
                Panel(Pretty(next_move, justify='center'), title='Step taken'),
                Panel(Pretty(next_pos, justify='center'), title='Next position'),
                Align.center(
                    Panel(step_history_table, title='Steps')
                )
            )
            layout['Movements'].update(Panel(log_group, title='Info'))

            curr_pos = next_pos
            steps.append(next_move)
            time.sleep(sleep_interval)

        layout['Labyrinth'].update(Panel(labyrinth.generate_table(curr_pos), title='Labyrinth'))
        time.sleep(2)

    # console.print(f'took {len(steps)} steps to collect {food_collected}/{total_food} food')
    return LabyrinthResult(food_collected, len(steps)), steps


def solve_labyrinth(labyrinth: Labyrinth,
                    total_food: int,
                    max_steps: int,
                    inherited_steps: list[Movement] = []) -> tuple[LabyrinthResult, list[Movement]]:
    steps: list[Movement] = []
    tried_steps = 0
    food_collected = 0
    curr_pos = Point(0, 0)
    tried_before = set()
    iter_inherited_steps = iter(inherited_steps)

    while tried_steps < max_steps and food_collected < total_food:
        next_move = next(iter_inherited_steps, heuristic(labyrinth, curr_pos))
        next_pos = move(curr_pos, next_move)

        if next_pos in tried_before:
            continue

        if not (next_pos.i >= 0 and next_pos.j >= 0):
            continue

        tried_steps += 1

        # Validate new position
        try:
            cell = labyrinth[next_pos.i][next_pos.j]
        except IndexError:
            # Invalid movement
            tried_before.add(next_pos)
            continue

        match cell:
            case Cell.Food:
                labyrinth[next_pos.i][next_pos.j] = Cell.Eaten
                food_collected += 1
            case Cell.Wall:
                # Invalid
                tried_before.add(next_pos)
                continue
            case Cell.Empty | Cell.Entry:
                # Do nothing
                pass

        tried_before = set()
        curr_pos = next_pos
        steps.append(next_move)

    return LabyrinthResult(food_collected, len(steps)), steps


def population_fitness(population: list[tuple[LabyrinthResult, list[Movement]]]) -> tuple[list[tuple[LabyrinthResult, list[Movement]]], float]:
    new_pop = deepcopy(population)

    # First, sort by amount of food gathered
    new_pop = sorted(new_pop, key=lambda item: item[0].food, reverse=True)

    # Then, sort by least amount of steps
    new_pop = sorted(new_pop, key=lambda item: item[0].steps)

    steps_mean = sum(map(lambda x: x[0].steps, new_pop)) / len(new_pop)
    return new_pop, steps_mean


def generate_random_labyrinth() -> Labyrinth:
    N = 10
    cells: list[list[Cell]] = []
    for i in range(N):
        cells.append([])
        for _ in range(N):
            cells[i].append(Cell[random.choice(list(Cell.__members__))])

    labirinth = Labyrinth(N, cells=cells)
    labirinth.print()
    return labirinth


def read_labyrinth_from_file(fname: str) -> Labyrinth:
    with open(fname) as f:
        lines = f.readlines()
    n = int(lines.pop(0))
    cells: list[list[Cell]] = []
    for i, line in enumerate(lines):
        cells.append([])
        file_cells = line.split(' ')
        for cell in file_cells:
            cell = cell.strip()
            match cell:
                case 'E':
                    cell_type = Cell.Entry
                case '1':
                    cell_type = Cell.Wall
                case '0':
                    cell_type = Cell.Empty
                case 'C':
                    cell_type = Cell.Food
                case _:
                    raise TypeError(cell)
            cells[i].append(cell_type)
    labyrinth = Labyrinth(n, cells)
    return labyrinth


def tournament_and_crossover(next_generation, population):
    while len(next_generation) < len(population):
        dad, mom = random.sample(population, 2)
        split_point = random.randint(0, min(len(dad[1]), len(mom[1]) - 1))
        child_1: list[Movement] = dad[1][:split_point] + mom[1][split_point:]
        child_2: list[Movement] = mom[1][:split_point] + dad[1][split_point:]

        # Mutate
        if random.randrange(100) < 10:
            amount = random.randint(1, len(child_1))
            for _ in range(amount):
                if random.randrange(100) < 5:
                    continue
                child_1[random.randint(0, len(child_1) - 1)] = random.choice(list(v for v in dict(Movement.__members__).values()))

        if random.randrange(100) < 10:
            amount = random.randint(1, len(child_2))
            for _ in range(amount):
                if random.randrange(100) < 5:
                    continue
                child_2[random.randint(0, len(child_2) - 1)] = random.choice(list(v for v in dict(Movement.__members__).values()))

        next_generation.append((LabyrinthResult(0, 0), child_1))
        next_generation.append((LabyrinthResult(0, 0), child_2))

    while len(next_generation) > len(population):
        next_generation.pop()


# Create log file
logs = open(f'execution_{LABYRINTH_FILE[:-4]}.log', 'w')

# Read labyrinth
ORIGINAL_LABYRINTH = read_labyrinth_from_file(LABYRINTH_FILE)

# Create first generation
population: list[tuple[LabyrinthResult, list[Movement]]] = []
write_log(logs, '-- Generation 0 --')
for i in range(MAX_POPULATION):
    lab = deepcopy(ORIGINAL_LABYRINTH)
    results = solve_labyrinth(lab, lab.n // 2, MAX_STEPS)
    population.append(results)

next_generation: list[tuple[LabyrinthResult, list[Movement]]] = []
aux_generation, avg = population_fitness(population)

# Elitism (n best)
[next_generation.append(candidate) for candidate in aux_generation[:ELITISM_BEST_AMOUNT]]

# Tournament and crossover
tournament_and_crossover(next_generation, population)

#? Write logs!
write_log(logs, f'-- Generation 0 --\n-- Average: {avg} -- Best: {next_generation[0][0]}')
console.log(f'-- Generation 0 -- Average: {avg} -- Best: {next_generation[0][0]}')
for i, chromo in enumerate(population):
    write_log(logs, f'[{i}]: Moves: ' + str(chromo[1]) + ' || Fitness: ' + str(chromo[0]))
write_log(logs, '\n')
#? Write logs!

# Start!
try:
    console.log('Starting and disabling KeyboardInterrupt')
    
    # Atomic operations
    kb_interrupt = signal.signal(signal.SIGINT, signal.SIG_IGN)
    
    for g in range(1, MAX_GENERATIONS):
        for i, chromo in enumerate(population):
            lab = deepcopy(ORIGINAL_LABYRINTH)
            results = solve_labyrinth(lab, lab.n // 2, MAX_STEPS, chromo[1])
            population[i] = results

        next_generation: list[tuple[LabyrinthResult, list[Movement]]] = []
        aux_generation, avg = population_fitness(population)

        # Elitism (n best)
        [next_generation.append(candidate) for candidate in aux_generation[:ELITISM_BEST_AMOUNT]]

        # Tournament and crossover
        tournament_and_crossover(next_generation, population)

        #? Write logs!
        write_log(logs, f'-- Generation {g} --\n-- Average: {avg} -- Best: {next_generation[0][0]}')
        console.log(f'-- Generation {g} -- Average: {avg} -- Best: {next_generation[0][0]}')
        for i, chromo in enumerate(population):
            write_log(logs, f'[{i}]: Moves: ' + str(chromo[1]) + ' || Fitness: ' + str(chromo[0]))
        write_log(logs, '\n')
        #? Write logs!

        population = next_generation

        # Enable KeyboardInterrupt
        signal.signal(signal.SIGINT, kb_interrupt)
except KeyboardInterrupt:
    pass

input('Press <ENTER> to see the resolution. It loops until you press Ctrl + C!')

while True:
    try:
        lab = deepcopy(ORIGINAL_LABYRINTH)
        _, best_steps = solve_step_by_step(lab, lab.n // 2, MAX_STEPS, inherited_steps=next_generation[0][1])
    except KeyboardInterrupt:
        break

logs.close()

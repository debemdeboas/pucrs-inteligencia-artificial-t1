---
author: Rafael Almeida de Bem
papersize: A4
colorlinks: yes
urlcolor: NavyBlue
geometry: "top=2cm, bottom=1.5cm, left=2cm, right=2cm"
header_includes:
    - \usepackage[brazilian]{babel}
date: \today
lang: pt-br
monofont: "Fira Code"
title: Trabalho 1 - Relatório
...

\tableofcontents

# Introdução

Este relatório visa explicar e exemplificar o código desenvolvido para o primeiro
trabalho da cadeira de Inteligência Artificial do curso de Ciência de Computação
da PUC-RS, semestre 2022/2.

O trabalho consiste em ler um labirinto de um arquivo e utilizar um algoritmo de
busca com informação por refinamentos sucessivos para encontrar uma solução
ótima.

## Algoritmo de busca com informação por refinamentos sucessivos

Escolhemos algoritmos genéticos como nosso algoritmo de busca.
Inicializamos uma população de tamanho *n* (`MAX_POPULATION`) e resolvemos os
labirintos com passos aleatórios sempre utilizando a função heurística.
Terminadas as resoluções, realizamos o cálculo da aptidão da população através
da função `population_fitness()`, que recebe a população por parâmetro e retorna
ela ordenada, assim como a média de passos daquela população.

O ordenamento da população se dá por duas ordenações seguidas:

1. Ordenamos inversamente por quantidade de comida encontrada (decrescente)
2. Feito isso, ordenamos pela quantidade de passos totais

Com isso, temos uma lista ordenada em "categorias" - nesse caso, o número de
comidas coletadas - onde cada "categoria" está ordenada por número total de
passos para encontrar aquela quantidade de comida.

Após a ordenação, pegamos os `ELITISM_BEST_AMOUNT` melhores e passamos para a próxima geração.
Depois fazemos operações de torneio, crossover e mutação.

O torneio é feito pegando dois membros aleatórios da população e escolhendo
aleatoriamente o ponto de corte entre os dois.
Como os cromossomos podem ter tamanhos diferentes, nosso ponto de corte vai de
zero até o menor dos tamanhos dos cromossomos.
São gerados dois filhos por crossover uniponto.
Feito o crossover, há uma chance de 10% de ocorrer mutação.
Caso ocorra mutação, é selecionado um número de um ao tamanho do filho e essa
vai ser a quantidade de genes que serão mutados.
Existe também uma chance de 5% de não ocorrer mutação daquele gene.

Com o torneio, crossover e mutações concluídos, nós ajustamos o tamanho da população
para que fique igual ao tamanho da população inicial pois podem ocorrer casos de
termos um ou até dois membros a mais na população. Esses membros a mais são descartados.
Feito isso, nós recomeçamos o algoritmo com a nova população.

# Codificação

O programa foi escrito em Python 3.10 e utiliza algumas bibliotecas de visualização
em linha de comando para facilitar o acompanhamento da execução do algoritmo por parte do usuário.
O arquivo `main.py` posui uma seção que define constantes de configuração da execução
como número máximo de passos, número máximo de gerações, número de membros por população,
entre outros.

## Classes e enumerações

Foram definidas algumas classes, enumerações e tuplas especiais para esse trabalho.
São elas:

- `Movement`, enumeração de tipos de movimentação
- `Cell`, conteúdo de uma célula do labirinto
- `LabyrinthResult`, resultado da função de resolução do labirinto
- `Point`, ponto cartesiano
- `Labyrinth`, que representa o labirinto com suas células, tamanho e
  funções auxiliares

### Enumerações

Foram criadas duas enumerações que representam uma movimentação e o conteúdo de
uma célula, respectivamente. A função `enum.auto()` permite que não atribuamos um
valor explícito à enumeração (como $1$, $2$).

```python
class Movement(enum.Enum):
    Left      = enum.auto()
    Right     = enum.auto()
    Up        = enum.auto()
    Down      = enum.auto()
    UpLeft    = enum.auto()
    UpRight   = enum.auto()
    DownLeft  = enum.auto()
    DownRight = enum.auto()

class Cell(enum.Enum):
    Empty = enum.auto()
    Wall  = enum.auto()
    Food  = enum.auto()
    Eaten = enum.auto()
    Entry = enum.auto()
```

### Tuplas (*NamedTuples*)

Em Python, `NamedTuples` são um tipo especial de tupla que possui nomes
para seus membros. Muito similares à `dataclasses`.
Utilizamos duas `NamedTuples` para representar informações simples:
os resultados totais de uma resolução de labirinto, e um ponto cartesiano.

```python
class LabyrinthResult(typing.NamedTuple):
    food: int
    steps: int

class Point(typing.NamedTuple):
    i: int
    j: int
```

A `LabyrinthResult` possui o número total de comida encontrada e o número total
de passos válidos dados.

### *Dataclasses*

Uma *dataclass* é uma classe sem construtor* (com um construtor implícito).
Utilizamos esse tipo de classe quando nós já possuímos os membros da classe
antes de inicializá-la.
A classe `Labyrinth` é desse tipo.

```python
@dataclass
class Labyrinth:
    n: int
    cells: list[list[Cell]]

    def __getitem__(self, index) -> list[Cell]: ...
    def generate_table(self, pos: Point) -> Table: ...
    def generate_table_no_coord(self, pos: Point) -> Table: ...
    def generate_table_with_coord(self, pos: Point) -> Table: ...
    def print(self): ...
```

# Função heurística

A função heurística utilizada considera apenas as células vizinhas à atual.
Isso porque não podíamos passar para o agente as informações completas do tabuleiro.
Também foi considerada uma função heurística que verificaria todas as células
da linha, coluna ou diagonal atual do agente (parando em paredes) - mas me
pareceu uma otimização prematura.

Após verificar as células vizinhas, cada tipo de célula é dado um peso:

|    Célula    | Peso |
| :----------: | :--: |
| `Cell.Empty` | $0$  |
| `Cell.Eaten` | $1$  |
| `Cell.Entry` | $2$  |

Note que as células do tipo `Cell.Food` e `Cell.Wall` não estão na tabela.
Isso porque caso a função encontre uma `Cell.Food` ele retorna imediatamente,
dando prioridade máxima à células com comida.
No caso de uma parede, a função nem as considera.

Assim que todas os vizinhos forem verificados, verificamos se a fila de células
ordenada por pesos não é vazia. Não sendo vazia, a função pega o primeiro item,
tenta pegar o segundo (caso exista), os embaralha e retorna o primeiro.
Caso a fila esteja vazia, um movimento aleatório é retornado.

# Problemas e outras considerações

A implementação da função de resolução do labirinto foi relativamente problemática
porque, em um primeiro momento, não havia sido pensada para "herança" de movimentos.
Cada geração herda de seus pais uma lista de passos/movimentos que foram tomados --
mas isso não havia sido definido quando a função foi concebida.

Outra dificuldade foi encontrar uma codificação dos cromossomos
apta a resolver o problema e herdável. Além disso, foi gasto muito tempo em
tentar encontrar um algoritmo de *crossover* (cruzamento) que suportasse
tamanhos de pais diferentes. No fim, optamos por utilizar cruzamento uniponto
onde o ponto de corte é um número aleatório de um ao menor tamanho do menor dos pais.

#!/bin/bash

cd doc
pandoc -o relatorio_ia.pdf --pdf-engine=lualatex -f markdown+implicit_figures relatorio_ia.md
exit $?
